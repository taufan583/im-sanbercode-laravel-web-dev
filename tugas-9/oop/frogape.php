<?php
require_once('animal.php');

class animalape extends animal
{
    public $leg = 2;
    public $name = "kera sakti";
    public function yell()
    {
        return "Aouuuoo <br><br>";
    }
}

class animalforg extends animal
{
    public $name = "kodok racun";
    public function jump()
    {
        return "Hop Hop <br><br>";
    }
}
