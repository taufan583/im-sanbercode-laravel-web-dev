<?php

require_once('animal.php');
require_once('frogape.php');

$hewan = new animal("domba");
echo "jenis hewan = " . $hewan->animal . "<br>";
echo "nama hewan = " . $hewan->name . "<br>"; // "shaun"
echo "jumlah kaki = " . $hewan->leg . "<br>"; // 4
echo "apakah berdarah dingin ? " . $hewan->cold_blooded . "<br><br>"; // "no

$kera = new animalape("kera");
echo "nama hewan = " . $kera->name . "<br>"; // "shaun"
echo "jumlah kaki = " . $kera->leg . "<br>"; // 4
echo "apakah berdarah dingin ? " . $kera->cold_blooded . "<br>"; // "no
echo "Yell = " . $kera->yell();

$kodok = new animalforg("kodok");
echo "nama hewan = " . $kodok->name . "<br>"; // "shaun"
echo "jumlah kaki = " . $kodok->leg . "<br>"; // 4
echo "apakah berdarah dingin ? " . $kodok->cold_blooded . "<br>"; // "no
echo "Jump = " . $kodok->jump();
