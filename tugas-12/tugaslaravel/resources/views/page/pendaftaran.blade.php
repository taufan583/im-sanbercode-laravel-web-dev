<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Buata Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="post">
        @csrf
        <label>Fist name:</label><br><br>
        <input type="text" name="fname" id=""><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lname" id=""><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="status" id="" value="1">Male <br>
        <input type="radio" name="status" id="" value="2">Female <br><br>
        <label>Nationality:</label><br><br>
        <select name="bahasa" id=""><br>
            <option value="1">Indonesia</option><br>
            <option value="2">Malaysia</option><br>
            <option value="3">Singapure</option><br>
            <option value="4">Thailand</option><br>
            <option value="5">Rusia</option><br>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="skil" id="" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="skil" id="" value="2">English <br>
        <input type="checkbox" name="skil" id="" value="3">Other <br><br>
        <label>Bio:</label><br><br>
        <textarea name="biodata" id="" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="Sign Up">
    </form>
</body>

</html>